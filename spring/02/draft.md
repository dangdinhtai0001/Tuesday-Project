Some related keywords

Aspect-Oriented Programming (AOP)
Spring AOP
Proxy Pattern
Dynamic Proxy
Java Dynamic Proxy
CGLIB Proxy
MethodInvocation
Advice
Before Advice
After Advice
Around Advice
After Returning Advice
After Throwing Advice
Pointcut
Joinpoint
Aspect
Weaving
Target Object
Proxy Object
Interceptor
Introduction (Mixin)
AspectJ
Spring Boot
Dependency Injection (DI)
Inversion of Control (IoC)

Aspect-Oriented Programming (AOP), Spring AOP, Proxy Pattern, Dynamic Proxy, Java Dynamic Proxy, CGLIB Proxy, MethodInvocation, Advice, Before Advice, After Advice, Around Advice, After Returning Advice, After Throwing Advice, Pointcut, Joinpoint, Aspect, Weaving, Target Object, Proxy Object, Interceptor, Introduction (Mixin), AspectJ, Spring Boot, Dependency Injection (DI), Inversion of Control (IoC)