- [ ] 01. Transaction trong Spring

---

- [ ] Spring Boot Performance Tuning: Các kỹ thuật và best practices
- [ ] Xây dựng ứng dụng Reactive với Spring WebFlux
- [ ] Spring Security: Tùy chỉnh xác thực và ủy quyền
- [ ] Sử dụng Spring Cloud Stream để xây dựng ứng dụng xử lý dữ liệu luồng
- [ ] Spring Boot và Kubernetes: Tích hợp và triển khai
- [ ] Spring Data: Tích hợp và tùy chỉnh với NoSQL Database
- [ ] Cấu hình động với Spring Cloud Config và Vault
- [ ] Thiết lập và quản lý Spring Cloud Gateway
- [ ] Spring Batch: Xây dựng và tối ưu hóa ứng dụng xử lý dữ liệu hàng loạt
- [ ] Spring Cloud Contract: Test-driven development trong kiến trúc Microservices
- [ ] Tích hợp Spring Boot với Apache Kafka và RabbitMQ
- [ ] Xây dựng ứng dụng GraphQL với Spring Boot
- [ ] Spring Cloud Sleuth và Zipkin: Giám sát và phân tích ứng dụng Microservices
- [ ] Xây dựng ứng dụng Multi-Tenant với Spring Boot
- [ ] Tích hợp Spring Boot với Caching Solutions: Redis, Hazelcast, Caffeine
- [ ] Sử dụng Spring Cloud Function để xây dựng ứng dụng Serverless
- [ ] Quản lý phiên bản API với Spring Boot và Spring Data REST
- [ ] Xây dựng ứng dụng gRPC với Spring Boot
- [ ] Xây dựng ứng dụng Spring Boot với Event Sourcing và CQRS
- [ ] Tích hợp Spring Boot với Elasticsearch và Logstash
- [ ] Spring Boot Admin: Giám sát và quản lý ứng dụng Spring Boot
- [ ] Xây dựng ứng dụng Microservices với Spring Cloud Netflix
- [ ] Tích hợp Spring Boot với Spring Cloud Data Flow
- [ ] Tối ưu hóa Spring Boot Database Connection Pooling
- [ ] Spring Boot và JPA: Các kỹ thuật tối ưu hóa và best practices
- [ ] Sử dụng Spring Cloud Circuit Breaker để xây dựng ứng dụng hệ thống chịu lỗi
- [ ] Spring Boot Test: Tích hợp và tùy chỉnh kiểm thử ứng dụng
- [ ] Tích hợp Spring Boot với Apache Camel và Spring Integration
- [ ] Xây dựng ứng dụng Microservices với Spring Boot và Quarkus
- [ ] Spring Boot và DevOps: Tích hợp Java vào quy trình CI/CD