tôi có outline sau, hãy ghi nhớ nó và đợi lệnh tiếp theo 
```
01. **Introduction**
    1.1 Overview of Software Design Patterns
    1.2 Importance of Object-Oriented Programming in design patterns
    1.3 Defining Behavioral Design Patterns

02. **Understanding the Observer Design Pattern**
    2.1 Introduction to the Observer Design Pattern
    2.2 Explanation of key terms: Observer, Subject, Listener
    2.3 The Publish-Subscribe Model: An overview
    2.4 Advantages of the Observer Pattern

...

```

tôi muốn dựa vào outline trên, sinh là 1 chuỗi các câu promt có cấu trúc như sau "Please proceed with composing the [number of section] section of the article, which should be titled "[name of section]" and specifically focus on subsection "[number of subsection].[name of section]". Your response should be detailed and comprehensive, covering all relevant aspects of the subject matter.". Biét [number of section], [name of section], [number of subsection], [name of section] là các giá trị tương ứng trên outline, lặp lại với tất cả các section và subsection của outline