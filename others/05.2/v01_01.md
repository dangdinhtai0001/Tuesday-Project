## 5. Authentication Flows

### Luồng Xác thực Mã Ủy quyền (Authorization Code Flow)

#### Tại sao Luồng Mã Ủy quyền lại quan trọng?
Luồng Mã Ủy quyền là một trụ cột trong OAuth 2.0 và OpenID Connect, đặc biệt phù hợp cho các ứng dụng web nơi mà an toàn và hiệu suất là những mối quan tâm hàng đầu. Thiết kế của nó phục vụ cho một kiến trúc bảo mật vững chắc, giảm thiểu rủi ro tiết lộ thông tin xác thực và token cho các thực thể xấu. Dòng chảy này là thiết yếu cho các nhà phát triển và kiến trúc sư nhằm hiểu các cơ chế xác thực người dùng và ủy quyền an toàn trong môi trường web hiện đại.

#### Cách thức hoạt động của Luồng Mã Ủy quyền
Cơ bản, Luồng Mã Ủy quyền bao gồm nhiều bước riêng biệt để xác thực người dùng và ủy quyền truy cập:

1. **Yêu cầu Ủy quyền**: Ứng dụng (thường là một ứng dụng web) sẽ chuyển hướng người dùng đến máy chủ ủy quyền. Yêu cầu này bao gồm danh tính của ứng dụng, phạm vi yêu cầu và URI chuyển hướng nơi máy chủ ủy quyền sẽ gửi người dùng sau khi cấp quyền truy cập.

2. **Xác thực Người dùng**: Sau khi nhận được yêu cầu ủy quyền, máy chủ ủy quyền sẽ xác thực người dùng. Quá trình xác thực này có thể bao gồm nhiều yếu tố tùy thuộc vào yêu cầu bảo mật của ứng dụng.

3. **Cấp Mã Ủy quyền**: Nếu việc xác thực người dùng thành công và người dùng đồng ý cấp quyền truy cập đã yêu cầu, máy chủ ủy quyền sẽ cấp một mã ủy quyền và gửi nó cho ứng dụng thông qua URI chuyển hướng.

4. **Trao đổi Token**: Sau đó, ứng dụng sẽ trao đổi mã ủy quyền lấy token truy cập (và tùy chọn là token làm mới) tại điểm cuối token của máy chủ ủy quyền. Trao đổi này cũng yêu cầu xác thực ứng dụng để đảm bảo yêu cầu được thực hiện bởi ứng dụng hợp lệ.

5. **Cấp Token Truy cập**: Máy chủ ủy quyền sẽ kiểm tra mã ủy quyền, thông tin xác thực của ứng dụng và URI chuyển hướng. Nếu hợp lệ, nó sẽ cấp một token truy cập và, tùy chọn, một token làm mới.

Dưới đây là biểu đồ minh họa cách thức hoạt động của nó:

```mermaid
sequenceDiagram
    participant Người Dùng as Người Dùng
    participant Ứng Dụng as Ứng Dụng Web
    participant Máy Chủ Ủy Quyền as Máy Chủ Ủy Quyền
    Ứng Dụng->>Máy Chủ Ủy Quyền: Yêu cầu ủy quyền (bao gồm phạm vi và URI chuyển hướng)
    Máy Chủ Ủy Quyền->>Người Dùng: Xác thực người dùng
    Người Dùng->>Máy Chủ Ủy Quyền: Cung cấp thông tin xác thực
    Máy Chủ Ủy Quyền->>Ứng Dụng: Gửi mã ủy quyền qua URI chuyển hướng
    Ứng Dụng->>Máy Chủ Ủy Quyền: Trao đổi mã ủy quyền lấy token truy cập (cần xác thực ứng dụng)
    Máy Chủ Ủy Quyền->>Ứng Dụng: Cấp token truy cập và token làm mới (tùy chọn)
```

#### Ví dụ Minh Họa
Giả sử bạn có một ứng dụng web giáo dục cho phép người dùng truy cập vào các khóa học trực tuyến. Ứng dụng này sử dụng Luồng Mã Ủy Quyền để xác thực người dùng và ủy quyền truy cập tài nguyên:

1. **Bắt đầu quá trình**: Người dùng chọn đăng nhập vào ứng dụng.
2. **Ứng dụng yêu cầu ủy quyền**: Ứng dụng chuyển hướng người dùng đến trang đăng nhập của máy chủ ủy quyền, kèm theo phạm vi truy cập yêu cầu và URI chuyển hướng.
3. **Xác thực người dùng**: Người dùng nhập thông tin đăng nhập của mình trên trang của máy chủ ủy quyền.
4. **Cấp mã ủy quyền**: Sau khi xác thực thành công, máy chủ ủy quyền chuyển hướng người dùng trở lại ứng dụng với mã ủy quyền.
5. **Trao đổi mã ủy quyền**: Ứng dụng gửi mã ủy quyền này đến máy chủ ủy quyền để trao đổi lấy token truy cập.
6. **Truy cập được ủy quyền**: Ứng dụng nhận token truy cập và sử dụng token này để yêu cầu tài nguyên từ máy chủ tài nguyên.

Thông qua ví dụ này, bạn có thể thấy rõ cách Luồng Mã Ủy Quyền hoạt động để bảo đảm an toàn thông tin và quyền truy cập của người dùng trong các ứng dụng web.

#### Thực hiện Luồng Mã Ủy quyền Một Cách An Toàn
Thực hiện Luồng Mã Ủy quyền một cách an toàn đòi hỏi sự chú ý đến một số phương pháp hay nhất:

- **Xác thực Ứng dụng**: Việc xác thực ứng dụng một cách vững chắc trong giai đoạn trao đổi token là rất quan trọng. Điều này có thể được thực hiện thông qua bí mật của ứng dụng, khẳng định của ứng dụng hoặc các phương thức xác thực khác được khuyến nghị bởi các tiêu chuẩn hiện tại.

- **URI Chuyển hướng An toàn**: Các URI chuyển hướng phải được đăng ký trước và kiểm tra với danh sách trắng để ngăn chặn các cuộc tấn công chuyển hướng nơi mã ủy quyền có thể bị bắt giữ.

- **Bộ Kiểm tra Mã**: Để tăng cường an toàn hơn nữa, đặc biệt là trong môi trường nơi ứng dụng không thể lưu trữ bí mật một cách an toàn (như ứng dụng di động hoặc ứng dụng trang đơn), việc thực hiện Chứng minh Khóa cho Trao đổi Mã (PKCE) được khuyến khích. PKCE thêm một lớp bảo vệ chống lại việc bắt giữ mã ủy quyền bằng cách yêu cầu xác minh mật mã bổ sung trong quá trình trao đổi token.

#### Kết Luận
Luồng Mã Ủy quyền là cần thiết cho việc xác thực và ủy quyền người dùng một cách an toàn và hiệu quả trong các ứng dụng web. Bằng cách hiểu và thực hiện dòng chảy này theo các phương pháp hay nhất, các nhà phát triển có thể đảm bảo an ninh vững chắc và trải nghiệm người dùng mượt mà. Dòng chảy này không chỉ bảo vệ dữ liệu nhạy cảm mà còn là nền tảng để tích hợp các biện pháp bảo mật nâng cao như PKCE, phù hợp với các tiêu chuẩn và thực tiễn bảo mật hiện đại.

### Luồng Mã Ủy Quyền với Chứng minh Khóa cho Trao đổi Mã (PKCE)

#### Tại sao PKCE lại quan trọng?
Chứng minh Khóa cho Trao đổi Mã (PKCE) được phát triển như một cơ chế bảo vệ bổ sung cho Luồng Mã Ủy Quyền, đặc biệt trong các ứng dụng không thể lưu giữ bí mật của ứng dụng một cách an toàn, như ứng dụng di động và ứng dụng trang đơn. PKCE giúp tăng cường bảo mật bằng cách ngăn chặn việc bắt giữ và sử dụng sai mã ủy quyền, một vấn đề an ninh quan trọng trong các luồng xác thực dựa trên chuyển hướng.

#### Cách thức hoạt động của PKCE
PKCE bắt đầu bằng việc tạo ra một bộ kiểm tra mã (code verifier) và một bộ thách thức mã (code challenge) bởi ứng dụng khách trước khi gửi yêu cầu đến máy chủ ủy quyền. Dưới đây là các bước cụ thể:

1. **Tạo Bộ Kiểm tra và Thách thức Mã**: Ứng dụng tạo ra một chuỗi ngẫu nhiên dài, được gọi là 'code verifier'. Sau đó, ứng dụng sử dụng thuật toán băm (thường là SHA256) để tạo ra 'code challenge' từ 'code verifier'.

2. **Yêu cầu Ủy quyền**: Khi yêu cầu ủy quyền từ máy chủ, ứng dụng sẽ gửi 'code challenge' này cùng với yêu cầu. Máy chủ ủy quyền lưu trữ 'code challenge' để sử dụng sau này trong quá trình xác minh.

3. **Người Dùng Xác Thực và Mã Ủy Quyền**: Người dùng được xác thực và máy chủ ủy quyền gửi mã ủy quyền về cho ứng dụng qua URI chuyển hướng.

4. **Trao đổi Token**: Khi trao đổi mã ủy quyền lấy token, ứng dụng cũng gửi 'code verifier'. Máy chủ ủy quyền sử dụng 'code verifier' để tạo lại 'code challenge' và so sánh nó với 'code challenge' đã lưu trước đó.

5. **Cấp Token Truy cập**: Nếu 'code challenge' khớp, điều này chứng minh rằng yêu cầu trao đổi token đến từ cùng một ứng dụng đã yêu cầu mã ủy quyền. Máy chủ ủy quyền sau đó cấp token truy cập cho ứng dụng.

Dưới đây là biểu đồ minh họa cách thức hoạt động của nó:

```mermaid
sequenceDiagram
    participant Người Dùng as Người Dùng
    participant Ứng Dụng as Ứng Dụng
    participant Máy Chủ Ủy Quyền as Máy Chủ Ủy Quyền
    Ứng Dụng->>Ứng Dụng: Tạo code verifier và code challenge
    Ứng Dụng->>Máy Chủ Ủy Quyền: Gửi code challenge và yêu cầu ủy quyền
    Máy Chủ Ủy Quyền->>Người Dùng: Xác thực người dùng
    Người Dùng->>Máy Chủ Ủy Quyền: Đồng ý cấp quyền
    Máy Chủ Ủy Quyền->>Ứng Dụng: Gửi mã ủy quyền qua URI chuyển hướng
    Ứng Dụng->>Máy Chủ Ủy Quyền: Gửi mã ủy quyền và code verifier
    Máy Chủ Ủy Quyền->>Máy Chủ Ủy Quyền: Xác minh code verifier
    Máy Chủ Ủy Quyền->>Ứng Dụng: Cấp token truy cập và token làm mới
```

#### Ví dụ Minh Họa
Giả sử có một ứng dụng di động giúp người dùng quản lý các giao dịch tài chính. Ứng dụng này sử dụng PKCE trong Luồng Mã Ủy Quyền để đảm bảo an toàn khi xác thực người dùng và trao đổi token:

1. **Khởi tạo PKCE**: Khi người dùng chọn đăng nhập, ứng dụng tạo một 'code verifier' ngẫu nhiên và từ đó sinh ra 'code challenge' sử dụng băm SHA256.
2. **Yêu cầu Ủy quyền**: Ứng dụng gửi 'code challenge' này đến máy chủ ủy quyền cùng với yêu cầu ủy quyền.
3. **Xác thực Người Dùng**: Người dùng nhập thông tin xác thực trên giao diện của máy chủ ủy quyền.
4. **Nhận Mã Ủy Quyền**: Sau khi xác thực thành công, máy chủ ủy quyền gửi mã ủy quyền trở lại ứng dụng qua URI chuyển hướng.
5. **Trao đổi Token**: Ứng dụng gửi mã ủy quyền và 'code verifier' đến máy chủ để trao đổi lấy token truy cập.
6. **Ủy quyền thành công**: Máy chủ ủy quyền xác minh 'code verifier', khớp với 'code challenge' đã lưu và cấp token truy cập cùng token làm mới cho ứng dụng.

Qua ví dụ này, bạn có thể thấy rằng PKCE tăng cường an toàn bằng cách đảm bảo rằng chỉ ứng dụng đã tạo ra mã ủy quyền mới có thể sử dụng mã đó để yêu cầu token, ngăn ngừa việc lạm dụng mã ủy quyền nếu nó bị bắt giữ bởi kẻ tấn công.

#### Tại sao PKCE lại cần thiết cho các Ứng dụng Di động và Trang đơn
PKCE đặc biệt quan trọng đối với các ứng dụng không thể an toàn để lưu trữ bí mật của ứng dụng vì nó giúp đảm bảo rằng chỉ ứng dụng đã yêu cầu mã ủy quyền mới có thể sử dụng mã đó để trao đổi token. Điều này ngăn ngừa việc kẻ tấn công có thể bắt giữ mã ủy quyền và sử dụng nó để lấy trái phép token truy cập.

#### Kết Luận
PKCE là một bổ sung thiết yếu cho Luồng Mã Ủy Quyền, cung cấp một lớp bảo mật thêm bằng cách đảm bảo rằng các token chỉ được cấp cho ứng dụng đã thực sự yêu cầu chúng. Điều này là cần thiết trong môi trường hiện đại nơi các ứng dụng thường xuyên phải đối mặt với các mối đe dọa từ các cuộc tấn công mạng. PKCE nên được cân nhắc là một phần không thể thiếu trong bất kỳ triển khai OAuth 2.0 và OpenID Connect nào cho ứng dụng di động hoặc trang đơn.

### Luồng Ngầm (Implicit Flow)

#### Tại sao Luồng Ngầm lại được sử dụng?
Trong quá khứ, Luồng Ngầm (Implicit Flow) được thiết kế cho các ứng dụng không có khả năng lưu trữ bảo mật thông tin xác thực, như các ứng dụng trang đơn (SPA - Single Page Applications). Luồng này trực tiếp cung cấp token truy cập (và/hoặc ID token) tới ứng dụng thông qua trình duyệt, mà không cần một bước trao đổi mã ủy quyền tại điểm cuối token, nhằm giảm thiểu độ trễ và phức tạp trong quá trình xác thực.

#### Cách thức hoạt động của Luồng Ngầm
Luồng Ngầm diễn ra như sau:

1. **Yêu cầu Token**: Ứng dụng gửi yêu cầu trực tiếp đến máy chủ ủy quyền, bao gồm danh tính ứng dụng và URI chuyển hướng, yêu cầu ID token và/hoặc token truy cập.

2. **Xác thực Người Dùng**: Người dùng được xác thực bởi máy chủ ủy quyền thông qua trình duyệt.

3. **Cấp Token**: Sau khi xác thực, máy chủ ủy quyền trả lời trực tiếp bằng cách chuyển hướng người dùng trở lại ứng dụng với token gắn trong URI chuyển hướng.

Dưới đây là biểu đồ minh họa cách thức hoạt động của Luồng Ngầm (Implicit Flow) trong OpenID Connect:

```mermaid
sequenceDiagram
    participant Người Dùng as Người Dùng
    participant Ứng Dụng as Ứng Dụng
    participant Máy Chủ Ủy Quyền as Máy Chủ Ủy Quyền
    Ứng Dụng->>Máy Chủ Ủy Quyền: Yêu cầu token (ID token và/hoặc Access token)
    Máy Chủ Ủy Quyền->>Người Dùng: Xác thực người dùng
    Người Dùng->>Máy Chủ Ủy Quyền: Đồng ý cấp quyền
    Máy Chủ Ủy Quyền->>Ứng Dụng: Chuyển hướng với token trong URI
```

#### Ví dụ Minh Họa
Giả sử bạn đang phát triển một ứng dụng trang đơn (SPA) để xem video trực tuyến. Ứng dụng này sử dụng Luồng Ngầm để xác thực người dùng và cấp quyền truy cập nhanh chóng vào API lấy thông tin video:

1. **Khởi tạo**: Người dùng nhấn vào nút "Đăng nhập" trên ứng dụng.
2. **Yêu cầu Token**: Ứng dụng chuyển hướng người dùng đến trang đăng nhập của máy chủ ủy quyền, yêu cầu trả về ID token và Access token.
3. **Xác thực và Phản hồi**: Người dùng đăng nhập và chấp nhận yêu cầu. Máy chủ ủy quyền sau đó chuyển hướng người dùng trở lại ứng dụng với ID token và Access token được gắn trực tiếp trong URI.
4. **Truy cập Nội dung**: Ứng dụng phân tích URI, thu thập token và sử dụng Access token để truy cập ngay lập tức vào API và lấy thông tin video mà người dùng muốn xem.

Thông qua việc sử dụng Luồng Ngầm, ứng dụng có thể cung cấp trải nghiệm người dùng nhanh chóng và liền mạch khi đăng nhập và truy cập vào nội dung, tuy nhiên, cần lưu ý rằng phương pháp này hiện không được khuyến khích do các vấn đề bảo mật đã nêu.

#### Hạn chế và Lý do để Hạn chế Sử dụng
Mặc dù Luồng Ngầm mang lại lợi ích về độ trễ thấp và tính đơn giản, nó không còn được khuyến khích sử dụng do những mối quan ngại về bảo mật:

- **Rò rỉ Token**: Token có thể bị rò rỉ thông qua lịch sử trình duyệt, bộ nhớ đệm, hoặc qua các tài nguyên của bên thứ ba được tải trong ứng dụng.
- **Cross-Site Scripting (XSS)**: Ứng dụng dễ bị tấn công XSS, có thể cho phép kẻ tấn công truy cập trái phép vào token.
- **Thiếu Tính Bảo Mật**: Không có khả năng thực hiện xác thực ứng dụng hoặc sử dụng bí mật khách hàng, khiến token dễ bị lạm dụng.

#### Sự Thay Thế và Lời Khuyên
Do những rủi ro bảo mật, các nhà phát triển hiện được khuyến khích sử dụng các luồng khác như Luồng Mã Ủy Quyền với PKCE, đặc biệt cho các ứng dụng trang đơn, để đảm bảo an toàn và hiệu quả hơn trong quản lý và bảo vệ token.

#### Kết Luận
Mặc dù Luồng Ngầm từng là một giải pháp phổ biến cho các ứng dụng không thể lưu giữ bí mật khách hàng, nó hiện nay được coi là lạc hậu do những lo ngại về bảo mật. Việc chuyển sang các phương pháp xác thực an toàn hơn là một bước tiến quan trọng trong việc bảo vệ thông tin người dùng và tài nguyên của hệ thống.

### Luồng Kết Hợp (Hybrid Flow)

#### Tại sao Luồng Kết Hợp lại quan trọng?
Luồng Kết Hợp trong OpenID Connect là một sự mở rộng của Luồng Mã Ủy Quyền, cung cấp khả năng phản hồi linh hoạt hơn bằng cách kết hợp các yếu tố của cả Luồng Mã Ủy Quyền và Luồng Ngầm (Implicit Flow). Điều này cho phép ứng dụng nhận cả mã ủy quyền và token (ID token và/hoặc access token) ngay trong phản hồi đầu tiên từ máy chủ ủy quyền, thay vì chỉ nhận mã ủy quyền hoặc token. Điều này là hữu ích trong các tình huống mà ứng dụng cần truy cập ngay lập tức vào thông tin người dùng hoặc khi việc truy cập API được yêu cầu ngay sau quá trình xác thực.

#### Cách thức hoạt động của Luồng Kết Hợp
Luồng Kết Hợp bắt đầu giống như Luồng Mã Ủy Quyền nhưng cho phép các loại phản hồi khác nhau tùy thuộc vào nhu cầu của ứng dụng. Các bước cơ bản trong Luồng Kết Hợp bao gồm:

1. **Yêu cầu Ủy quyền**: Ứng dụng gửi yêu cầu đến máy chủ ủy quyền, chỉ định một loại phản hồi mong muốn, thường là một sự kết hợp của `code` (mã ủy quyền), `id_token` (token danh tính), và/hoặc `token` (token truy cập).

2. **Xác thực Người Dùng**: Người dùng được xác thực bởi máy chủ ủy quyền.

3. **Phản hồi từ Máy Chủ Ủy Quyền**: Tùy thuộc vào yêu cầu phản hồi, máy chủ ủy quyền sẽ gửi một hoặc nhiều loại token hoặc mã trở lại cho ứng dụng thông qua URI chuyển hướng.

4. **Trao đổi Mã Ủy Quyền (nếu có)**: Nếu mã ủy quyền được cung cấp, ứng dụng sẽ trao đổi mã này cho một access token tại điểm cuối token của máy chủ ủy quyền.

5. **Sử Dụng Token**: Ứng dụng có thể sử dụng ID token để lấy thông tin về người dùng và access token để truy cập các dịch vụ hoặc tài nguyên được bảo vệ.

Dưới đây là biểu đồ minh họa các bước hoạt động của Luồng Kết Hợp (Hybrid Flow):

```mermaid
sequenceDiagram
    participant Người Dùng as Người Dùng
    participant Ứng Dụng as Ứng Dụng
    participant Máy Chủ Ủy Quyền as Máy Chủ Ủy Quyền
    Ứng Dụng->>Máy Chủ Ủy Quyền: Yêu cầu ủy quyền (code, id_token, token)
    Máy Chủ Ủy Quyền->>Người Dùng: Xác thực người dùng
    Người Dùng->>Máy Chủ Ủy Quyền: Đồng ý cấp quyền
    Máy Chủ Ủy Quyền->>Ứng Dụng: Gửi mã ủy quyền, ID token, và/hoặc Access token
    alt Mã Ủy Quyền có mặt
        Ứng Dụng->>Máy Chủ Ủy Quyền: Trao đổi mã ủy quyền lấy Access token
        Máy Chủ Ủy Quyền->>Ứng Dụng: Cấp Access token mới
    end
```

#### Ví dụ Minh Họa
Hãy tưởng tượng một ứng dụng mạng xã hội cho phép người dùng đăng nhập và ngay lập tức hiển thị thông tin cá nhân cũng như cập nhật trạng thái mới nhất. Ứng dụng này sử dụng Luồng Kết Hợp để tăng tốc quá trình xác thực và truy cập dữ liệu:

1. **Khởi tạo**: Người dùng chọn đăng nhập qua ứng dụng mạng xã hội.
2. **Yêu cầu Ủy Quyền**: Ứng dụng chuyển hướng người dùng đến trang đăng nhập của máy chủ ủy quyền và yêu cầu cả mã ủy quyền và token (ID token và Access token).
3. **Xác thực và Phản hồi**: Người dùng xác thực danh tính của mình và đồng ý chia sẻ dữ liệu. Máy chủ ủy quyền sau đó gửi trở lại mã ủy quyền, ID token (chứa thông tin người dùng) và Access token (cho phép truy cập API).
4. **Sử dụng Token**: Ngay khi nhận token, ứng dụng có thể sử dụng ID token để hiển thị thông tin người dùng và sử dụng Access token để lấy dữ liệu trạng thái mới nhất từ API.

Thông qua việc sử dụng Luồng Kết Hợp, ứng dụng có thể nhanh chóng và an toàn trong việc xác thực người dùng và đồng thời cung cấp truy cập ngay lập tức vào các tài nguyên cần thiết, mang lại trải nghiệm người dùng liền mạch và hiệu quả.

#### Khi nào nên sử dụng Luồng Kết Hợp?
Luồng Kết Hợp đặc biệt hữu ích trong các ứng dụng phức tạp cần truy cập ngay lập tức cả token và mã ủy quyền. Điều này có thể bao gồm các ứng dụng doanh nghiệp lớn có yêu cầu bảo mật cao, các ứng dụng tương tác nhanh với API sau khi người dùng đăng nhập, hoặc khi một ứng dụng cần xác thực người dùng và đồng thời yêu cầu quyền truy cập vào các tài nguyên mà không muốn chờ đợi một bước trao đổi mã.

#### Kết Luận
Luồng Kết Hợp cung cấp một giải pháp linh hoạt và hiệu quả cho các ứng dụng cần phản ứng nhanh chóng và an toàn từ quá trình xác thực đến truy cập tài nguyên. Bằng cách kết hợp các yếu tố của cả Luồng Mã Ủy Quyền và Luồng Ngầm, nó mở ra nhiều khả năng cho các nhà phát triển để đáp ứng các nhu cầu đa dạng của ứng dụng trong bối cảnh bảo mật hiện đại.

### Luồng Ủy Quyền Thiết Bị (Device Authorization Flow)

#### Tại sao Luồng Ủy Quyền Thiết Bị lại quan trọng?
Luồng Ủy Quyền Thiết Bị được thiết kế để xử lý các tình huống xác thực trong đó thiết bị không có khả năng nhập liệu dễ dàng hoặc không có trình duyệt web tích hợp, như là smart TVs, máy chơi game, và các thiết bị IoT. Phương thức này giúp người dùng xác thực thiết bị trên một thiết bị khác có khả năng nhập liệu và trình duyệt tốt hơn, như một điện thoại di động hoặc máy tính, qua một mã được cung cấp bởi thiết bị.

#### Cách thức hoạt động của Luồng Ủy Quyền Thiết Bị
Luồng Ủy Quyền Thiết Bị bao gồm các bước sau:

1. **Khởi tạo Ủy Quyền**: Thiết bị gửi yêu cầu đến điểm cuối ủy quyền của máy chủ (device authorization endpoint) để lấy một mã thiết bị (device code) và mã người dùng (user code).

2. **Hiển thị Mã Người Dùng**: Mã người dùng được hiển thị trên thiết bị, hướng dẫn người dùng đến một URL cụ thể trên một thiết bị khác để nhập mã này.

3. **Xác Thực bởi Người Dùng**: Người dùng sử dụng thiết bị khác để truy cập URL đã cho, nhập mã người dùng, và xác thực.

4. **Polling**: Trong khi người dùng đang thực hiện xác thực, thiết bị thường xuyên gửi yêu cầu đến điểm cuối token của máy chủ (token endpoint) với mã thiết bị để kiểm tra trạng thái xác thực.

5. **Cấp Token**: Khi xác thực thành công, máy chủ cấp token truy cập cho thiết bị.

Dưới đây là biểu đồ minh họa các bước hoạt động của Luồng Ủy Quyền Thiết Bị (Device Authorization Flow) trong OpenID Connect:

```mermaid
sequenceDiagram
    participant Thiết Bị as Thiết Bị
    participant Máy Chủ Ủy Quyền as Máy Chủ Ủy Quyền
    participant Người Dùng as Người Dùng
    participant Thiết Bị Khác as Thiết Bị Khác (Ví dụ: Điện thoại)
    Thiết Bị->>Máy Chủ Ủy Quyền: Yêu cầu mã thiết bị và mã người dùng
    Máy Chủ Ủy Quyền->>Thiết Bị: Cung cấp mã thiết bị và mã người dùng
    Thiết Bị->>Người Dùng: Hiển thị mã người dùng và URL
    Người Dùng->>Thiết Bị Khác: Nhập mã tại URL đã cho
    Thiết Bị Khác->>Máy Chủ Ủy Quyền: Xác thực mã người dùng
    loop Kiểm Tra Trạng Thái
        Thiết Bị->>Máy Chủ Ủy Quyền: Polling với mã thiết bị
        Máy Chủ Ủy Quyền->>Thiết Bị: Kiểm tra xác thực
    end
    Máy Chủ Ủy Quyền->>Thiết Bị: Cấp token truy cập
```

#### Ví dụ Minh Họa
Hãy tưởng tượng bạn có một smart TV muốn truy cập vào các dịch vụ streaming nhạc trực tuyến như Spotify. Smart TV sử dụng Luồng Ủy Quyền Thiết Bị để xác thực người dùng:

1. **Khởi tạo Ủy Quyền**: Khi bạn chọn đăng nhập vào Spotify trên smart TV, TV sẽ gửi yêu cầu đến máy chủ ủy quyền của Spotify để lấy mã thiết bị và mã người dùng.

2. **Hiển thị Mã và URL**: TV hiển thị mã người dùng và URL mà người dùng cần truy cập trên thiết bị khác, chẳng hạn như điện thoại di động hoặc máy tính.

3. **Xác Thực Người Dùng**: Người dùng sử dụng điện thoại di động để truy cập URL đã cho, nhập mã người dùng và xác thực.

4. **Polling và Nhận Token**: Trong khi người dùng thực hiện các bước xác thực trên điện thoại, smart TV thực hiện polling đến máy chủ ủy quyền với mã thiết bị để kiểm tra trạng thái xác thực. Khi xác thực hoàn tất, máy chủ ủy quyền sẽ gửi token truy cập về TV.

5. **Truy Cập Dịch Vụ**: Smart TV sử dụng token truy cập này để đăng nhập vào Spotify và người dùng có thể bắt đầu streaming nhạc.

Thông qua việc sử dụng Luồng Ủy Quyền Thiết Bị, smart TV có thể cung cấp trải nghiệm đăng nhập thuận tiện và an toàn cho người dùng, mà không yêu cầu nhập liệu trực tiếp trên thiết bị có khả năng nhập liệu hạn chế.

#### Ứng Dụng Thực Tế của Luồng Ủy Quyền Thiết Bị
Luồng này rất hữu ích cho các thiết bị như smart TVs hay thiết bị âm thanh thông minh, nơi người dùng có thể muốn xem nội dung truyền hình hoặc nghe nhạc qua các dịch vụ yêu cầu xác thực, nhưng lại khó khăn trong việc nhập thông tin xác thực trực tiếp trên thiết bị.

#### Kết Luận
Luồng Ủy Quyền Thiết Bị mang lại giải pháp xác thực tiện lợi và an toàn cho các thiết bị có hạn chế về khả năng nhập liệu hoặc hiển thị. Nó cho phép người dùng xác thực các thiết bị một cách dễ dàng thông qua thiết bị khác, giúp kết nối an toàn với các dịch vụ trực tuyến mà không cần thao tác phức tạp trên thiết bị chính.

### Luồng Công Thức của Khách Hàng (Client Credentials Flow)

#### Tại sao Luồng Công Thức của Khách Hàng lại quan trọng?
Luồng Công Thức của Khách Hàng (Client Credentials Flow) được thiết kế cho các tình huống mà ứng dụng cần truy cập vào các dịch vụ hoặc tài nguyên được bảo vệ mà không cần hoạt động thay mặt cho một người dùng cụ thể. Điều này thường xảy ra trong các tương tác máy-tới-máy (M2M), nơi các ứng dụng hoặc dịch vụ backend cần giao tiếp với nhau qua API.

#### Cách thức hoạt động của Luồng Công Thức của Khách Hàng
Luồng này rất đơn giản và thực hiện các bước sau:

1. **Yêu cầu Token**: Ứng dụng (client) gửi yêu cầu đến điểm cuối token của máy chủ ủy quyền (authorization server) với bí mật của khách hàng và các thông tin xác thực khác.

2. **Cấp Token**: Máy chủ ủy quyền xác thực các thông tin của khách hàng và, nếu hợp lệ, cấp một access token.

Dưới đây là biểu đồ minh họa các bước hoạt động của Luồng Công Thức của Khách Hàng (Client Credentials Flow) trong OpenID Connect:

```mermaid
sequenceDiagram
    participant Ứng Dụng as Ứng Dụng
    participant Máy Chủ Ủy Quyền as Máy Chủ Ủy Quyền
    Ứng Dụng->>Máy Chủ Ủy Quyền: Gửi thông tin xác thực khách hàng và yêu cầu token
    Máy Chủ Ủy Quyền->>Ứng Dụng: Xác thực và cấp Access token
```

#### Ví dụ Minh Họa
Hãy tưởng tượng một hệ thống quản lý dữ liệu lớn cho một công ty phân tích dữ liệu. Hệ thống này tự động lấy dữ liệu từ một dịch vụ thời tiết API để phân tích và dự báo thời tiết, và cần truy cập vào API mà không thông qua sự tương tác của người dùng:

1. **Khởi tạo Yêu cầu**: Hệ thống backend của công ty phân tích dữ liệu gửi yêu cầu đến máy chủ ủy quyền của dịch vụ thời tiết, đi kèm với bí mật khách hàng và thông tin xác thực khác.

2. **Nhận Token**: Máy chủ ủy quyền xác minh thông tin và cấp một access token cho hệ thống backend.

3. **Truy cập Dữ liệu**: Với access token, hệ thống backend sau đó gọi API dịch vụ thời tiết, sử dụng token để ủy quyền các yêu cầu và thu thập dữ liệu thời tiết.

Thông qua việc sử dụng Luồng Công Thức của Khách Hàng, hệ thống phân tích dữ liệu có thể tự động và an toàn truy cập vào các dữ liệu cần thiết từ các dịch vụ ngoài mà không cần can thiệp hoặc xác thực liên tục từ người dùng.

#### Ứng Dụng Thực Tế của Luồng Công Thức của Khách Hàng
Luồng này phù hợp cho các tình huống như các dịch vụ backend cần truy cập vào một API để lấy dữ liệu cần thiết cho các hoạt động nội bộ hoặc để thực hiện các tác vụ tự động mà không cần sự tương tác trực tiếp từ người dùng.

#### Kết Luận
Luồng Công Thức của Khách Hàng cung cấp một cách tiếp cận hiệu quả và an toàn cho các ứng dụng cần truy cập không dựa trên người dùng vào các tài nguyên API. Nó là lý tưởng cho các trường hợp mà xác thực và ủy quyền diễn ra giữa các hệ thống mà không liên quan trực tiếp đến người dùng cuối.

### Luồng Mật Khẩu của Chủ Sở Hữu Tài Nguyên (Resource Owner Password Flow)

#### Tại sao Luồng Mật Khẩu của Chủ Sở Hữu Tài Nguyên lại được sử dụng?
Luồng Mật Khẩu của Chủ Sở Hữu Tài Nguyên, hay còn gọi là Resource Owner Password Flow, ban đầu được thiết kế để xử lý các tình huống mà người dùng tin tưởng ứng dụng đến mức họ sẵn sàng cung cấp thông tin đăng nhập của mình trực tiếp. Trong luồng này, người dùng cung cấp tên người dùng và mật khẩu trực tiếp cho ứng dụng, và ứng dụng sau đó sử dụng thông tin đó để yêu cầu token từ máy chủ ủy quyền.

#### Cách thức hoạt động của Luồng Mật Khẩu của Chủ Sở Hữu Tài Nguyên
Luồng này hoạt động qua các bước sau:

1. **Nhập Thông Tin Đăng Nhập**: Người dùng nhập tên người dùng và mật khẩu vào ứng dụng.
2. **Yêu cầu Token**: Ứng dụng gửi thông tin đăng nhập cùng với yêu cầu token đến máy chủ ủy quyền.
3. **Cấp Token**: Nếu thông tin đăng nhập chính xác, máy chủ ủy quyền cấp access token và, tùy chọn, refresh token cho ứng dụng.

Dưới đây là biểu đồ minh họa các bước hoạt động của Luồng Mật Khẩu của Chủ Sở Hữu Tài Nguyên (Resource Owner Password Flow) trong OpenID Connect:

```mermaid
sequenceDiagram
    participant Người Dùng as Người Dùng
    participant Ứng Dụng as Ứng Dụng
    participant Máy Chủ Ủy Quyền as Máy Chủ Ủy Quyền
    Người Dùng->>Ứng Dụng: Cung cấp tên người dùng và mật khẩu
    Ứng Dụng->>Máy Chủ Ủy Quyền: Gửi thông tin đăng nhập và yêu cầu token
    Máy Chủ Ủy Quyền->>Ứng Dụng: Xác thực và cấp Access token
```

#### Ví dụ Minh Họa
Giả sử bạn đang phát triển một ứng dụng di động cho phép người dùng truy cập vào các tài liệu nội bộ của công ty. Ban đầu, ứng dụng này sử dụng Luồng Mật Khẩu của Chủ Sở Hữu Tài Nguyên để xác thực người dùng:

1. **Nhập Thông Tin Đăng Nhập**: Người dùng mở ứng dụng và nhập tên người dùng và mật khẩu của họ vào giao diện đăng nhập.
2. **Gửi Thông Tin Đăng Nhập**: Ứng dụng gửi thông tin này đến máy chủ ủy quyền của công ty.
3. **Nhận Token**: Máy chủ ủy quyền kiểm tra thông tin đăng nhập và, nếu chính xác, gửi lại một access token cho ứng dụng.
4. **Truy Cập Tài Liệu**: Ứng dụng sử dụng access token này để yêu cầu tải các tài liệu nội bộ từ máy chủ tài liệu của công ty.

Thông qua ví dụ này, bạn có thể thấy rằng Luồng Mật Khẩu của Chủ Sở Hữu Tài Nguyên cung cấp một phương pháp đơn giản và trực tiếp để xác thực, nhưng nó mang lại rủi ro bảo mật do lưu trữ và truyền tải thông tin đăng nhập dễ bị tấn công. Do đó, việc sử dụng các phương thức xác thực an toàn hơn như Authorization Code Flow với PKCE là điều được khuyến khích trong các ứng dụng hiện đại.

#### Hạn chế và Lý do Giảm Sử Dụng
Mặc dù luồng này có vẻ thuận tiện, nó mang lại nhiều rủi ro bảo mật và không còn được khuyến khích sử dụng:

- **Rò rỉ Thông Tin Xác Thực**: Thông tin đăng nhập của người dùng có thể bị rò rỉ hoặc bị đánh cắp nếu ứng dụng không đảm bảo bảo mật thông tin một cách cẩn thận.
- **Thiếu Phân Tách Ủy Quyền và Xác Thực**: Việc cung cấp mật khẩu trực tiếp cho ứng dụng làm mờ đi ranh giới giữa xác thực và ủy quyền, điều này có thể dẫn đến các vấn đề về quản trị và bảo mật.
- **Sự Phụ Thuộc vào Mật Khẩu**: Luồng này phụ thuộc hoàn toàn vào mật khẩu, điều này không phù hợp với xu hướng hiện đại hướng tới các phương thức xác thực mạnh mẽ hơn và đa yếu tố.

#### Sự Thay Thế và Lời Khuyên
Để tăng cường bảo mật, các ứng dụng nên tránh sử dụng luồng này và thay vào đó, sử dụng các luồng khác như Authorization Code Flow với PKCE, đặc biệt là trong các ứng dụng mà người dùng cần một mức độ bảo mật cao hơn.

#### Kết Luận
Trong khi Luồng Mật Khẩu của Chủ Sở Hữu Tài Nguyên từng phù hợp cho một số tình huống đặc thù, sự phát triển của các tiêu chuẩn bảo mật hiện đại và sự nhận thức ngày càng tăng về rủi ro bảo mật đã dẫn đến việc giảm dần sự phụ thuộc vào luồng này. Các nhà phát triển nên cân nhắc kỹ lưỡng trước khi áp dụng luồng này và hướng tới các phương pháp ủy quyền an toàn hơn.