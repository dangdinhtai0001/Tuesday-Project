Bytecode, Just-In-Time Compiler (JIT) và GraalVM
================================================

Mục lục
-------

*   I. Giới thiệu
*   II. Giới thiệu về Java Bytecode
    *   A. Định nghĩa và vai trò của Java Bytecode
    *   B. Cấu trúc và mã hóa của Java Bytecode
    *   C. Đọc và hiểu Bytecode
*   III. Cách hoạt động của Just-In-Time Compiler
    *   A. Quá trình biên dịch JIT
    *   B. Tối ưu hóa mã trong quá trình biên dịch JIT
    *   C. Lợi ích và nhược điểm của JIT
*   IV. HotSpot JIT Compiler
    *   A. Giới thiệu về HotSpot JIT Compiler
    *   B. Tính năng và cách hoạt động của HotSpot JIT Compiler
*   V. GraalVM - Một nền tảng JIT và AOT mới
    *   A. Giới thiệu về GraalVM
    *   B. GraalVM JIT Compiler
    *   C. GraalVM AOT Compiler
    *   D. Các tính năng và ưu điểm của GraalVM
*   VI. So sánh HotSpot và GraalVM
*   VII. Kết luận
*   VIII. Tài liệu tham khảo