# Garbage Collection (GC) trong JVM

## 1. Giới thiệu
## 2. Bản chất và mục đích của Garbage Collection
## 3. Các thuật toán GC phổ biến
### 3.1. Serial GC
### 3.2. Parallel GC
### 3.3. Concurrent Mark Sweep (CMS) GC
### 3.4. Garbage First (G1) GC
### 3.5. Z Garbage Collector (ZGC)
### 3.6. Shenandoah GC
## 4. Cách tinh chỉnh và giám sát GC để tối ưu hiệu suất ứng dụng
### 4.1. Các thông số quan trọng liên quan đến GC
### 4.2. Cách tinh chỉnh các thông số GC
### 4.3. Giám sát và phân tích báo cáo GC
### 4.4. Thực hành tối ưu hiệu suất ứng dụng
## 5. Kết luận
## 6. Tài liệu tham khảo