# Garbage Collection (GC) trong JVM

## 1. Giới thiệu
Garbage Collection (GC) là một phần quan trọng của Java Virtual Machine (JVM) và đóng vai trò thiết yếu trong việc quản lý bộ nhớ cho các ứng dụng Java. GC giúp giải phóng bộ nhớ khi các đối tượng không còn được sử dụng, đảm bảo rằng các ứng dụng Java chạy ổn định và hiệu quả trong môi trường JVM.

Trong bài viết này, chúng ta sẽ tìm hiểu về bản chất và mục đích của Garbage Collection, các thuật toán GC phổ biến và cách tinh chỉnh cũng như giám sát GC để tối ưu hiệu suất ứng dụng.

## 2. Bản chất và mục đích của Garbage Collection

Trong chương này, chúng ta sẽ tìm hiểu về bản chất của Garbage Collection (GC) và tại sao nó lại quan trọng đối với việc quản lý bộ nhớ trong Java. Chúng ta sẽ đi sâu vào các khái niệm cơ bản và lợi ích của việc sử dụng GC trong quá trình chạy ứng dụng Java.

### 2.1. Bản chất của Garbage Collection

Garbage Collection (GC) là một quá trình tự động quản lý bộ nhớ trong các ngôn ngữ lập trình như Java. Mục đích chính của GC là giải phóng bộ nhớ không còn được sử dụng bởi chương trình, giúp tránh tình trạng thiếu bộ nhớ (memory leaks) và làm cho việc lập trình trở nên dễ dàng hơn.

Trong các ngôn ngữ lập trình không có hỗ trợ GC, việc quản lý bộ nhớ trở nên phức tạp và gây nên lỗi khó tìm và sửa chữa. Ngược lại, GC giúp giảm bớt gánh nặng cho lập trình viên, cho phép họ tập trung vào việc phát triển tính năng của ứng dụng mà không phải lo lắng về việc quản lý bộ nhớ một cách chi tiết.

### 2.2. Mục đích của Garbage Collection

- **Quản lý bộ nhớ**: Garbage Collection (GC) giúp quản lý bộ nhớ hiệu quả bằng cách tự động giải phóng bộ nhớ không còn được sử dụng, giúp tránh tình trạng lãng phí tài nguyên hệ thống. Khi các đối tượng không còn được sử dụng, chúng trở thành "rác" và Garbage Collector sẽ quét bộ nhớ heap để xác định các đối tượng này và giải phóng bộ nhớ của chúng. Quá trình này giúp giảm áp lực về quản lý bộ nhớ của ứng dụng và đảm bảo rằng bộ nhớ có sẵn để sử dụng cho các đối tượng mới.

- **Phát hiện và giải phóng các đối tượng không còn được sử dụng**: Việc phát hiện các đối tượng không còn được sử dụng và giải phóng chúng khỏi bộ nhớ heap là một chức năng quan trọng của Garbage Collection. Khi các đối tượng không còn được tham chiếu bởi bất kỳ đối tượng nào trong chương trình, chúng trở thành rác và không còn được sử dụng nữa. Nếu không có Garbage Collection, bộ nhớ heap sẽ bị chiếm dụng bởi các đối tượng rác và dần dần đầy, dẫn đến tình trạng tràn bộ nhớ (memory leak). Garbage Collection giúp giải quyết vấn đề này bằng cách tự động giải phóng bộ nhớ heap khi không còn có đối tượng nào tham chiếu đến các đối tượng rác.
- **Tối ưu hiệu suất ứng dụng**: GC đảm bảo sự ổn định và hiệu suất của ứng dụng Java. Tuy nhiên, quá trình GC hoạt động có thể gây ra độ trễ (latency) trong quá trình thực thi ứng dụng, đặc biệt là trong các ứng dụng yêu cầu phản hồi nhanh hoặc các hệ thống có tải cao. Vì vậy, tối ưu hiệu suất GC là một yếu tố quan trọng trong việc cải thiện hiệu suất của ứng dụng Java. Các kỹ thuật tinh chỉnh GC có thể bao gồm sử dụng các thuật toán GC khác nhau, tùy chỉnh các thông số cấu hình của GC, hoặc sử dụng các công cụ hỗ trợ để phân tích hiệu suất GC và cải thiện các vấn đề liên quan đến độ trễ.
- **Hỗ trợ lập trình viên**: GC giúp giảm bớt trách nhiệm quản lý bộ nhớ cho lập trình viên. Thay vì phải thủ công cấp phát và giải phóng bộ nhớ cho các đối tượng trong ứng dụng, lập trình viên có thể tập trung vào phát triển chức năng và tính năng của ứng dụng mà không cần quan tâm đến việc quản lý bộ nhớ. Tuy nhiên, lập trình viên cũng cần hiểu cách GC hoạt động và tối ưu mã của họ để tránh các vấn đề liên quan đến hiệu suất và trễ.

Trong phần tiếp theo, chúng ta sẽ tìm hiểu về các thuật toán GC phổ biến và cách chúng hoạt động để đạt được các mục tiêu trên.

## 3. Các thuật toán GC phổ biến

Trong phần này, chúng ta sẽ tìm hiểu về các thuật toán GC phổ biến được sử dụng trong Java Virtual Machine (JVM). Mỗi thuật toán có những đặc điểm và ưu nhược điểm riêng, phù hợp với các mục tiêu và yêu cầu khác nhau của ứng dụng Java. Hiểu rõ về các thuật toán GC sẽ giúp lập trình viên lựa chọn phương pháp tối ưu cho ứng dụng của mình.

Có nhiều thuật toán GC được thiết kế để đáp ứng nhu cầu khác nhau của các ứng dụng Java. Mỗi thuật toán GC được xây dựng dựa trên một số nguyên tắc cơ bản như đánh dấu và dọn dẹp, nhưng có sự khác biệt về cách thức hoạt động, mục tiêu tối ưu và ảnh hưởng đến hiệu suất của ứng dụng. Các thuật toán GC phổ biến bao gồm:

### 3.1. Serial GC

Được giới thiệu trong JDK 1.3, thuật toán Serial GC là một trong những thuật toán GC cổ điển nhất và đơn giản nhất trong JVM. Nó sử dụng một luồng (thread) cho việc thu thập rác và dừng toàn bộ ứng dụng trong quá trình thu gom rác, gọi là stop-the-world (STW). Khi thực hiện STW, toàn bộ ứng dụng sẽ bị tạm dừng, bao gồm cả luồng chính (main thread), cho đến khi quá trình thu gom rác hoàn tất. Kết quả là tất cả các luồng (thread) đều bị tạm dừng trong khoảng thời gian ngắn.

Thuật toán Serial GC được sử dụng mặc định trong các ứng dụng Java nhỏ và đơn giản, không yêu cầu xử lý dữ liệu lớn và đòi hỏi tài nguyên bộ nhớ thấp. Tuy nhiên, nó không phù hợp cho các ứng dụng lớn và phức tạp, bởi vì thời gian STW lâu và có thể gây ra các vấn đề về hiệu suất.

Trong thuật toán Serial GC, quá trình thu gom rác được thực hiện theo hai giai đoạn: Mark và Sweep.

- Giai đoạn Mark: Trong giai đoạn này, Garbage Collector đánh dấu (mark) các đối tượng được sử dụng bằng cách đệ quy thông qua tất cả các đối tượng bắt đầu từ đối tượng root (các biến static, biến local, và đối tượng bắt đầu từ stack). Các đối tượng được đánh dấu (mark) sẽ không bị thu gom rác.
- Giai đoạn Sweep: Trong giai đoạn này, Garbage Collector sẽ quét toàn bộ heap và giải phóng bộ nhớ của các đối tượng không được đánh dấu (unmarked).

Tổng thời gian thực hiện quá trình thu gom rác của thuật toán Serial GC sẽ tăng theo tỷ lệ với kích thước bộ nhớ heap, và đặc biệt là trong giai đoạn Sweep. Khi thực hiện quá trình Sweep, JVM phải tìm kiếm toàn bộ heap để giải phóng bộ nhớ của các đối tượng không được đánh dấu, điều này có thể làm tăng độ phức tạp và thời gian thực hiện của quá trình thu gom rác.

### 3.2. Parallel GC

Parallel GC là một thuật toán GC sử dụng multiple threads để thu thập rác trên bộ nhớ heap. Nó được thiết kế để tận dụng các bộ vi xử lý có nhiều lõi, giúp tăng tốc độ thu gom rác bằng cách sử dụng nhiều thread đồng thời.

Trong quá trình hoạt động của Parallel GC, việc thu thập rác được thực hiện trong các giai đoạn. Trong giai đoạn đầu tiên, các đối tượng được đánh dấu để xác định những đối tượng nào sẽ bị thu gom. Trong giai đoạn thứ hai, các đối tượng được thu gom và bộ nhớ của chúng được giải phóng.

Parallel GC có thể được cấu hình để sử dụng một số lượng thread cố định hoặc tự động điều chỉnh số lượng thread dựa trên số lượng CPU hiện có. Điều này giúp tối ưu hóa hiệu suất thu gom rác và giảm thiểu độ trễ của ứng dụng.

Tuy nhiên, do sử dụng nhiều thread để thực hiện thu gom rác, Parallel GC có thể gây ra tình trạng sử dụng CPU cao và gây ảnh hưởng đến hiệu suất của các tiến trình khác trên hệ thống.

### 3.3. Concurrent Mark Sweep (CMS) GC

Concurrent Mark Sweep (CMS) GC là một thuật toán thu gom rác được thiết kế để giảm thiểu độ trễ (latency) trong quá trình thu gom rác bằng cách sử dụng nhiều luồng để thực hiện việc thu gom rác. CMS GC được sử dụng phổ biến trong các ứng dụng yêu cầu thời gian đáp ứng nhanh và có thời gian dừng (stop-the-world time) thấp.

CMS GC được thực hiện trong các giai đoạn sau:

- **Initial Mark**: JVM tạm dừng tất cả các luồng thực thi và xác định các đối tượng trực tiếp được tham chiếu bởi các biến tĩnh, biến stack và các thanh ghi CPU. Các đối tượng này được đánh dấu là "live".
- **Concurrent Mark**: JVM bắt đầu chạy các luồng thu gom rác để xác định các đối tượng "live" khác bằng cách truy xuất các đối tượng đã được đánh dấu trong Initial Mark. Quá trình này được thực hiện song song với luồng chính của ứng dụng.
- **Remark**: JVM tạm dừng tất cả các luồng thực thi và xác định các đối tượng "live" mới được tham chiếu trong quá trình Concurrent Mark.
- **Concurrent Sweep**: JVM bắt đầu chạy các luồng thu gom rác để giải phóng các đối tượng không còn được sử dụng. Quá trình này được thực hiện song song với luồng chính của ứng dụng.

Tuy nhiên, CMS GC cũng có một số hạn chế. Việc sử dụng nhiều luồng trong quá trình thu gom rác có thể làm tăng độ phức tạp của thuật toán và tốn nhiều tài nguyên hệ thống. CMS GC cũng có thể gây ra tình trạng Fragmentation (phân mảnh) bộ nhớ, dẫn đến sự lãng phí tài nguyên và giảm hiệu suất của ứng dụng.

### 3.4. Garbage First (G1) GC

Garbage First (G1) GC là một thuật toán GC mới được giới thiệu từ JDK 7u4. G1 GC được thiết kế để giải quyết các vấn đề về hiệu suất và khả năng mở rộng của CMS GC, đồng thời giảm thiểu độ trễ (latency) trong quá trình thu gom rác.

Thuật toán G1 GC được chia thành các vùng (regions) bằng cách chia bộ nhớ heap thành các khu vực bằng nhau. Khi quá trình thu gom rác bắt đầu, G1 GC sẽ tập trung vào các vùng có nhiều đối tượng rác, đảm bảo rằng các vùng này sẽ được thu gom trước. Điều này giúp giảm thiểu độ trễ và tối ưu hóa việc sử dụng bộ nhớ.

G1 GC sử dụng các vòng lặp thu gom rác ngắn hơn và phân tán các hoạt động thu gom rác trong nhiều chu kỳ thu gom rác. Các chu kỳ này được điều khiển bởi JVM và được thiết kế để đảm bảo rằng các vùng được thu gom rác trước khi cạn kiệt bộ nhớ.

G1 GC có thể giảm thiểu độ trễ và tăng hiệu suất bằng cách cho phép xử lý đa luồng trong quá trình thu gom rác. Nó cũng có khả năng xử lý các tệp heap lớn hơn mà không ảnh hưởng đến hiệu suất.

Tuy nhiên, G1 GC có thể tốn nhiều thời gian để tối ưu hóa và phù hợp với các ứng dụng có yêu cầu bộ nhớ heap lớn. Nó cũng có thể ảnh hưởng đến hiệu suất nếu các vùng được chia quá nhỏ hoặc nếu số lượng đối tượng rác quá ít.

### 3.5. Z Garbage Collector (ZGC)

Z Garbage Collector (ZGC) là một thuật toán thu gom rác của Oracle được giới thiệu trong Java 11. ZGC được thiết kế để hỗ trợ các ứng dụng Java đòi hỏi tính khả dụng cao và thời gian phản hồi thấp. Điều này đặc biệt quan trọng trong các ứng dụng có thời gian phản hồi thấp, chẳng hạn như các ứng dụng trò chơi hoặc các ứng dụng xử lý dữ liệu theo thời gian thực.

Thuật toán ZGC được thiết kế để hoạt động với các bộ nhớ rất lớn, đến hàng trăm gigabyte hoặc thậm chí là terabyte. Nó sử dụng một số kỹ thuật mới như tối ưu hóa bộ nhớ và điều khiển tỉ lệ bộ nhớ, và đồng thời vẫn duy trì khả năng tùy chỉnh cao. Một số tính năng chính của ZGC bao gồm:

- Tự động điều chỉnh kích thước bộ nhớ để đảm bảo tính khả dụng cao.
- Giảm đáng kể thời gian phản hồi trong quá trình thu gom rác.
- Duy trì khả năng tùy chỉnh cao, cho phép người dùng điều chỉnh các tham số để phù hợp với nhu cầu của ứng dụng.
- Hỗ trợ các hệ thống có nhiều nhân xử lý (multithreading), giúp tăng tốc độ xử lý.
  
Tuy nhiên, ZGC cũng có một số hạn chế, chẳng hạn như tốc độ thu gom rác không bằng G1 GC và phải sử dụng một số lượng bộ nhớ tạm để lưu trữ các đối tượng được gán nhãn.

### 3.6. Shenandoah GC

Shenandoah GC là một thuật toán GC mới được giới thiệu vào năm 2018 bởi Oracle. Nó là một thuật toán GC thuộc loại concurrent, có khả năng giảm thiểu độ trễ và tối ưu hóa hiệu suất của ứng dụng.

Shenandoah GC được thiết kế để hoạt động với bất kỳ kích thước heap nào và có khả năng đáp ứng yêu cầu của các ứng dụng với tải lớn và độ trễ thấp. Nó hoạt động bằng cách chia heap thành các vùng con và thực hiện thu gom rác trên từng vùng con một.

Thuật toán Shenandoah GC sử dụng một phương pháp đánh dấu trường hợp đặc biệt để xác định các đối tượng có thể bị thu gom mà không cần quét toàn bộ heap. Nó sử dụng một bảng tham chiếu để giữ các tham chiếu đến các đối tượng được đánh dấu và sử dụng bảng này để quét các vùng con của heap.

Shenandoah GC có khả năng hoạt động đồng thời với các luồng thực thi của ứng dụng, vì vậy nó không gây ra độ trễ đáng kể trong quá trình thu gom rác. Nó cũng có khả năng thích ứng với các trường hợp tải lớn và độ trễ nhỏ mà không cần thiết lập các thông số GC trước đó.

Một ưu điểm của Shenandoah GC là nó có khả năng tối ưu hóa hiệu suất của ứng dụng trong các môi trường có nhiều bộ xử lý và bộ nhớ, như trong các hệ thống máy chủ với nhiều CPU và RAM. Nó cũng có khả năng giảm thiểu độ trễ cho các ứng dụng đòi hỏi độ trễ thấp.

Tuy nhiên, một số nhược điểm của Shenandoah GC là nó có thể tăng độ trễ trong quá trình khởi động của JVM và có thể yêu cầu bộ nhớ phụ trợ để lưu trữ bảng tham chiếu. Nó cũng có thể gây ra độ trễ nhất định trong quá trình xử lý các đối tượng được đánh dấu và trong quá trình compacting heap.

Trong phần tiếp theo, chúng ta sẽ tìm hiểu về cách tinh chỉnh và giám sát GC để tối ưu hiệu suất ứng dụng Java.

## 4. Cách tinh chỉnh và giám sát GC để tối ưu hiệu suất ứng dụng

Trong phần này, chúng ta sẽ tập trung vào việc tinh chỉnh và giám sát Garbage Collection (GC) để tối ưu hiệu suất ứng dụng Java. Việc tinh chỉnh GC đúng cách giúp cải thiện hiệu suất, độ trễ và sự ổn định của ứng dụng. Bên cạnh đó, giám sát GC là một công cụ quan trọng để giúp lập trình viên phát hiện các vấn đề liên quan đến bộ nhớ và hiệu suất, từ đó đưa ra giải pháp tối ưu.

### 4.1. Giám sát GC

Sử dụng công cụ giám sát bên ngoài: Có nhiều công cụ giám sát GC phổ biến như VisualVM, JConsole, GCViewer, đều hỗ trợ giám sát GC trong JVM. Công cụ này giúp theo dõi thông tin về hoạt động của GC, thời gian dừng, bộ nhớ heap, các khu vực bộ nhớ, và nhiều thông tin khác.

Sử dụng các tùy chọn JVM: JVM cung cấp nhiều tùy chọn để giám sát và ghi lại thông tin về hoạt động GC. Một số tùy chọn phổ biến bao gồm: -verbose:gc, -XX:+PrintGCDetails, -XX:+PrintGCDateStamps, -Xloggc:<file-path>.

### 4.2. Tinh chỉnh GC

Tinh chỉnh GC đòi hỏi việc hiểu rõ về thuật toán GC, cấu trúc bộ nhớ heap và đặc điểm của ứng dụng. Dưới đây là một số gợi ý để tinh chỉnh GC:

- **Lựa chọn thuật toán GC phù hợp**: Cân nhắc các thuật toán GC khác nhau và lựa chọn thuật toán phù hợp với nhu cầu và đặc điểm của ứng dụng. Ví dụ, nếu ứng dụng yêu cầu thời gian dừng GC ngắn, có thể lựa chọn G1GC hoặc ZGC.
- **Điều chỉnh kích thước bộ nhớ heap**: Điều chỉnh kích thước bộ nhớ heap phù hợp với ứng dụng, đảm bảo cung cấp đủ bộ nhớ cho các đối tượng và hạn chế số lần GC xảy ra. Sử dụng các tùy chọn JVM như -Xms, -Xmx, -XX:NewSize, -XX:MaxNewSize, -XX:SurvivorRatio,...
- **Theo dõi và đánh giá hiệu suất GC**: Sử dụng các công cụ giám sát và tùy chọn JVM để theo dõi hiệu suất GC, phát hiện các vấn đề về hiệu suất, thời gian dừng GC dài, tràn bộ nhớ, và thực hiện các điều chỉnh cần thiết.
- **Tối ưu hóa mã nguồn:** Đôi khi, tối ưu hóa mã nguồn ứng dụng có thể giảm bớt áp lực lên GC, giúp cải thiện hiệu suất. Điều này bao gồm việc sử dụng các cấu trúc dữ liệu phù hợp, hạn chế sử dụng đối tượng tạm thời, và loại bỏ các tham chiếu không cần thiết.

## 5. Kết luận 

Trong bài viết này, chúng ta đã tìm hiểu về Garbage Collection (GC) trong Java Virtual Machine (JVM) và vai trò của nó trong quá trình quản lý bộ nhớ. Chúng ta đã khám phá bản chất và mục đích của GC, cũng như các thuật toán GC phổ biến như Serial, Parallel, CMS, G1, ZGC và Shenandoah. Ngoài ra, chúng ta cũng đã đi sâu vào cách tinh chỉnh và giám sát GC để tối ưu hiệu suất ứng dụng Java.

Hiểu về GC và cách tinh chỉnh nó là rất quan trọng đối với các nhà phát triển Java, bởi vì GC ảnh hưởng đến hiệu suất, độ trễ và độ tin cậy của ứng dụng. Bằng cách áp dụng kiến thức về GC, các nhà phát triển có thể xây dựng các ứng dụng hiệu quả hơn và đảm bảo rằng chúng hoạt động mượt mà trong môi trường sản xuất.

Hy vọng bài viết này sẽ giúp bạn có cái nhìn tổng quan và sâu sắc hơn về GC trong JVM, cũng như cung cấp cho bạn các công cụ và kỹ thuật cần thiết để tối ưu hiệu suất ứng dụng Java của mình.

## 6. Tài liệu tham khảo

1.  Oracle. (2021). _Java SE Documentation: Garbage Collection_.  [https://docs.oracle.com/en/java/javase/11/gctuning/index.html](https://docs.oracle.com/en/java/javase/11/gctuning/index.html)
2.  Oracle. (2021). _Understanding Java Garbage Collection_.  [https://www.oracle.com/java/technologies/understanding-java-garbage-collection.html](https://www.oracle.com/java/technologies/understanding-java-garbage-collection.html)
3.  Red Hat. (2021). _Shenandoah GC_.  [https://wiki.openjdk.java.net/display/shenandoah/Main](https://wiki.openjdk.java.net/display/shenandoah/Main)
4.  Shipilev, A. (2021). _JEP 333: ZGC: A Scalable Low-Latency Garbage Collector_.  [https://openjdk.java.net/jeps/333](https://openjdk.java.net/jeps/333)
5.  Goetz, B. (2013). _Java theory and practice: Urban performance legends_.  [https://www.ibm.com/developerworks/library/j-jtp04223/](https://www.ibm.com/developerworks/library/j-jtp04223/)
6.  Verburg, M. (2021). _Java Garbage Collection Basics_.  [https://www.oracle.com/webfolder/technetwork/tutorials/obe/java/gc01/index.html](https://www.oracle.com/webfolder/technetwork/tutorials/obe/java/gc01/index.html)
7.  Java Performance Tuning. (2021). _Java Garbage Collection tuning_.  [http://www.javaperformancetuning.com/tips/gc.shtml](http://www.javaperformancetuning.com/tips/gc.shtml)
8.  Ponge, J. (2021). _Java Garbage Collection Distilled_.  [https://malloc.se/blog/java\_gc\_distilled](https://malloc.se/blog/java_gc_distilled)
9.  Peierls, T., Goetz, B., Bloch, J., Bowbeer, J., & Holmes, D. (2005). _Java Concurrency in Practice_. Addison-Wesley.
