- [x] 01. Giới thiệu về Kubernetes và kiến trúc của nó: Khám phá các thành phần chính và mối quan hệ giữa chúng
- [x] 02. Cài đặt và cấu hình môi trường Kubernetes trên Linux, Windows và Cloud
- [x] 03. Quản lý tài nguyên Kubernetes: Tìm hiểu về Pod, Deployment, Service, và ReplicaSet
- [ ] 04. Network trong Kubernetes
- [x] 05. Kubernetes Ingress và các ứng dụng của nó
- [ ] 06. 
- [ ] 07. 
- [ ] 08. Kubernetes Runtime Security: Giám sát và phát hiện xâm nhập
- [ ] 09. Giải thích về Network Policies trong Kubernetes
- [ ] 10. Pod Lifecycle trong Kubernetes
- [x] 11. Quản lý lưu trữ hiệu quả trong Kubernetes

---


- Kiến trúc và thành phần trong Kubernetes: Hiểu về control plane, node components và cách chúng hoạt động cùng nhau.
- Tìm hiểu về Kubernetes API server và cách tùy chỉnh API objects để quản lý cluster.
- Khái niệm etcd trong Kubernetes: Lưu trữ dữ liệu cấu hình, trạng thái của cluster, và cách tối ưu hóa hiệu suất.
- Kubernetes scheduler: Nguyên tắc hoạt động, thuật toán lập lịch và cách tùy chỉnh.
- Kubernetes Controller Manager và tùy chỉnh các loại controllers.
- Triển khai ứng dụng với Kubernetes: Sử dụng Deployment, StatefulSets và DaemonSets.
- Quản lý storage trong Kubernetes: Persistent Volumes, Persistent Volume Claims và StorageClasses.
- Tìm hiểu về Kubernetes Services và Ingress để cung cấp mạng và cân bằng tải cho ứng dụng.
- Tối ưu hóa hiệu suất và khả năng chịu tải bằng Kubernetes Horizontal Pod Autoscaler và Cluster Autoscaler.
- Kubernetes Resource Quotas và LimitRange: Quản lý tài nguyên và giới hạn sử dụng tài nguyên cho namespaces.
- Bảo mật trong Kubernetes: Xác thực và ủy quyền, Kubernetes RBAC, Pod Security Policies.
- Kubernetes Network Security: Network Policies, CNI Plugins và Encryption.
- Tích hợp Kubernetes với dịch vụ AWS: Amazon EKS, Elastic Load Balancing, RDS, S3.
- Sử dụng Amazon ECR để lưu trữ và quản lý Docker images cho ứng dụng Kubernetes.
- Tích hợp Kubernetes với AWS IAM để quản lý quyền truy cập dựa trên vai trò.
- Giám sát và quản lý hệ thống Kubernetes: Prometheus, Grafana, ELK Stack.
- Kubernetes logging: Fluentd, Elasticsearch, Kibana và tích hợp với Amazon CloudWatch.
- Kubernetes auditing: Audit logs, API server auditing và Amazon GuardDuty.
- Cấu hình và quản lý ứng dụng Kubernetes: ConfigMaps, Secrets, Environment Variables.
- Sử dụng Helm để đóng gói, triển khai và quản lý ứng dụng Kubernetes.
- Canary deployments và blue-green deployments trong Kubernetes để triển khai ứng dụng an toàn và linh hoạt.
- Tìm hiểu về Kubernetes Custom Resource Definitions (CRDs) và cách mở rộng API server.
- Kubernetes Operators: Xây dựng và quản lý ứng dụng thông qua Custom Resource và Custom Controller.
- Tích hợp Kubernetes với dịch vụ AWS Lambda để xây dựng ứng dụng serverless.
- Kubernetes Federation: Quản lý nhiều cluster Kubernetes và cung cấp khả năng chịu tải
- Kubernetes và GitOps: Áp dụng nguyên tắc GitOps để tự động hóa quy trình triển khai và quản lý ứng dụng trong Kubernetes.
- Quản lý dịch vụ phụ thuộc trong Kubernetes bằng cách sử dụng Service Mesh, ví dụ như Istio và Linkerd.
- Tối ưu hóa hiệu suất mạng trong Kubernetes: Các phương pháp tăng tốc độ truyền dữ liệu giữa Pods và Services.
- Tích hợp Kubernetes với Amazon RDS Proxy để tối ưu hóa hiệu suất và khả năng chịu tải của cơ sở dữ liệu.
- Sử dụng Amazon FSx for Lustre để cung cấp hệ thống tập tin chịu tải cao (High Performance File System) cho ứng dụng Kubernetes.



