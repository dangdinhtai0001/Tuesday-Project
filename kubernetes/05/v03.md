# Kubernetes Ingress và các ứng dụng của nó

## 1. Giới thiệu về Kubernetes Ingress

Kubernetes là một nền tảng mã nguồn mở phổ biến, được thiết kế để tự động triển khai, mở rộng và quản lý các ứng dụng container hóa. Trong bối cảnh này, Ingress đóng vai trò quan trọng như một cầu nối giữa các ứng dụng chạy trong môi trường Kubernetes và người dùng cuối.

### 1.1. Định nghĩa Ingress

Ingress trong Kubernetes là một API object cung cấp quy tắc định tuyến để điều hướng lưu lượng mạng bên ngoài vào các service bên trong Kubernetes cluster. Nói cách khác, Ingress cho phép bạn kiểm soát việc truy cập vào các ứng dụng chạy trong cluster thông qua một địa chỉ IP hoặc tên miền. Ingress giúp cải thiện khả năng mở rộng, bảo mật và quản lý của ứng dụng.

### 1.2. Tầm quan trọng của Ingress trong Kubernetes

Ingress đóng vai trò quan trọng trong việc kiểm soát việc truy cập vào ứng dụng, nó cho phép bạn cấu hình một số tính năng quan trọng như load balancing, SSL termination và phân tách lưu lượng dựa trên đường dẫn hoặc tên miền. Ngoài ra, Ingress cũng giúp bạn tối ưu hóa hiệu suất và bảo mật cho ứng dụng của mình.

### 1.3. Mục đích của việc sử dụng Ingress

Một số mục đích chính của việc sử dụng Ingress trong Kubernetes bao gồm:

- Điều hướng lưu lượng mạng từ bên ngoài vào các service bên trong cluster.
- Cung cấp các quy tắc định tuyến linh hoạt dựa trên đường dẫn hoặc tên miền.
- Tự động cân bằng tải giữa các pods chạy ứng dụng.
- Cung cấp khả năng kết thúc SSL và mở rộng bảo mật cho ứng dụng.
- Tối ưu hóa hiệu suất và quản lý tài nguyên.

Trong bài viết này, chúng ta sẽ tìm hiểu về các thành phần chính của Kubernetes Ingress, cách cài đặt và cấu hình Ingress, cũng như các ứng dụng phổ biến của Ingress trong thực tế.

## 2. Các thành phần chính của Ingress

Ingress trong Kubernetes bao gồm hai thành phần chính: Ingress Controller và Ingress Resource. Trong phần này, chúng ta sẽ tìm hiểu về từng thành phần này và vai trò của chúng trong việc điều hướng lưu lượng mạng.

### 2.1. Ingress Controller

#### 2.1.1. Định nghĩa Ingress Controller

Ingress Controller là một thành phần quan trọng của Kubernetes Ingress, chịu trách nhiệm thực thi các quy tắc định tuyến được xác định trong Ingress Resource. Nói cách khác, Ingress Controller là một ứng dụng chạy trong Kubernetes cluster, giám sát các Ingress Resource và cập nhật cấu hình của mình để phản ánh các thay đổi trong các quy tắc định tuyến.

#### 2.1.2. Các loại Ingress Controller phổ biến

Có nhiều loại Ingress Controller được hỗ trợ trong Kubernetes, mỗi loại sử dụng một phần mềm load balancer khác nhau để cân bằng tải và điều hướng lưu lượng mạng. Dưới đây là một số Ingress Controller phổ biến:

- Nginx Ingress Controller: Sử dụng Nginx làm reverse proxy và load balancer.
- Traefik Ingress Controller: Sử dụng Traefik, một proxy ngược và load balancer hiện đại, dễ sử dụng và linh hoạt.
- HAProxy Ingress Controller: Sử dụng HAProxy, một phần mềm load balancing phổ biến.
- Envoy Ingress Controller: Sử dụng Envoy, một proxy ngược và load balancer hiệu năng cao được thiết kế cho môi trường container và microservices.

### 2.2. Ingress Resource

#### 2.2.1. Định nghĩa Ingress Resource

Ingress Resource là một đối tượng API trong Kubernetes, chứa các quy tắc định tuyến để điều hướng lưu lượng mạng từ bên ngoài vào các service trong cluster. Ingress Resource giúp bạn cấu hình các định tuyến dựa trên đường dẫn, tên miền, cũng như cấu hình bảo mật SSL/TLS cho ứng dụng của bạn.

#### 2.2.2. Cấu trúc và cách sử dụng Ingress Resource

Cấu trúc của một Ingress Resource bao gồm các thông tin sau:

- Metadata: Chứa tên và namespace của Ingress Resource, cũng như các annotations để tùy chỉnh cấu hình Ingress Controller.
- Spec: Chứa các quy tắc định tuyến và cấu hình SSL/TLS cho ứng dụng.

Một ví dụ về Ingress Resource đơn giản:

```yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: my-ingress
  namespace: my-namespace
  annotations:
    kubernetes.io/ingress.class: "nginx"
spec:
  rules:
  - host: example.com
    http:
      paths:
      - path: /app1
        pathType: Prefix
        backend:
          service:
            name: app1-service
            port:
              number: 80
      - path: /app2
        pathType: Prefix
        backend:
          service:
            name: app2-service
            port:
              number: 80
  tls:
  - hosts:
    - example.com
    secretName: example-tls
```

### 2.3. Annotations và Customizations

#### 2.3.1. Annotations trong Ingress

Annotations là một cách để tùy chỉnh cấu hình của Ingress Controller mà không cần thay đổi mã nguồn. Bằng cách thêm các annotations vào metadata của Ingress Resource, bạn có thể thay đổi hành vi của Ingress Controller và áp dụng các cấu hình đặc biệt cho ứng dụng của mình.

Ví dụ:

```yaml
metadata:
  name: my-ingress
  annotations:
    nginx.ingress.kubernetes.io/rewrite-target: /
    nginx.ingress.kubernetes.io/ssl-redirect: "true"
```

Trong ví dụ trên, chúng ta sử dụng annotations để cấu hình Nginx Ingress Controller chuyển hướng URL và buộc sử dụng SSL.

#### 2.3.2. Các ví dụ về Customizations

Dưới đây là một số ví dụ về việc sử dụng annotations để tùy chỉnh cấu hình của Ingress Controller:

- Tùy chỉnh thời gian chờ kết nối (connection timeout): nginx.ingress.kubernetes.io/proxy-connect-timeout
- Cấu hình rate limiting: nginx.ingress.kubernetes.io/limit-rps
- Cấu hình caching: nginx.ingress.kubernetes.io/proxy-cache-path
  
Việc sử dụng các annotations và tùy chỉnh Ingress Controller giúp bạn tận dụng tối đa tính năng của Ingress và cung cấp một môi trường ổn định, hiệu năng cao cho ứng dụng của mình.

### 2.4. Custom Resource Definitions (CRDs) liên quan đến Ingress

Một số Ingress Controller, như Traefik và Kong, hỗ trợ sử dụng Custom Resource Definitions (CRDs) để cung cấp tính năng mở rộng và tùy chỉnh hơn cho Ingress. CRDs cho phép bạn định nghĩa các đối tượng Kubernetes mới dựa trên nhu cầu của ứng dụng, giúp bạn tận dụng các tính năng đặc biệt của Ingress Controller đó.

Ví dụ về CRD trong Traefik Ingress Controller:

```yaml
apiVersion: traefik.containo.us/v1alpha1
kind: Middleware
metadata:
  name: my-middleware
spec:
  stripPrefix:
    prefixes:
      - /app1
```

Trong ví dụ trên, chúng ta định nghĩa một CRD kiểu Middleware cho Traefik Ingress Controller, giúp gỡ bỏ tiền tố đường dẫn khi chuyển hướng đến service bên trong.

## 3. Cài đặt và cấu hình Ingress

Trong phần này, chúng ta sẽ tìm hiểu cách cài đặt và cấu hình Ingress trong môi trường Kubernetes. Đặc biệt, chúng ta sẽ tập trung vào việc cài đặt Istio Ingress Controller.

### 3.1. Cài đặt Istio Ingress Controller

Istio là một service mesh mã nguồn mở, cung cấp các tính năng quan trọng như bảo mật, quan sát, và điều hướng lưu lượng cho các ứng dụng được triển khai trong Kubernetes. Istio Ingress Controller sử dụng Envoy Proxy để cân bằng tải và điều hướng lưu lượng mạng.

#### 3.1.1. Cài đặt Istio

Trước tiên, bạn cần cài đặt Istio trên Kubernetes cluster của bạn. Bạn có thể làm theo hướng dẫn chính thức tại [đây](https://istio.io/latest/docs/setup/getting-started/).

#### 3.1.2. Kích hoạt Istio Ingress Gateway

Istio Ingress Gateway cung cấp chức năng Ingress Controller cho Istio. Để kích hoạt Istio Ingress Gateway, bạn cần cấu hình Istio Operator, một công cụ giúp cài đặt và quản lý Istio trên Kubernetes cluster. Để kích hoạt Istio Ingress Gateway, bạn cần thêm đoạn sau vào cấu hình Istio Operator:

```yaml
apiVersion: install.istio.io/v1alpha1
kind: IstioOperator
metadata:
  namespace: istio-system
spec:
  profile: default
  components:
    ingressGateways:
    - name: istio-ingressgateway
      enabled: true
```

Sau đó, áp dụng cấu hình mới này:

```bash 
kubectl apply -f istio-operator.yaml
```

#### 3.1.3. Kiểm tra Istio Ingress Controller

Để kiểm tra Istio Ingress Controller đã được cài đặt thành công, bạn có thể chạy lệnh sau:

```bash
kubectl get svc -n istio-system
```

Nếu thành công, bạn sẽ thấy service istio-ingressgateway được liệt kê trong kết quả.

### 3.2. Cấu hình Ingress Resource cho Istio Ingress Controller

Sau khi cài đặt Istio Ingress Controller, bạn cần cấu hình Ingress Resource để sử dụng Istio Ingress Gateway. Với Istio, bạn sẽ sử dụng các đối tượng Gateway và VirtualService để cấu hình Ingress.

#### 3.2.1. Tạo đối tượng Gateway

Tạo một tệp YAML với nội dung sau:

```yaml
apiVersion: networking.istio.io/v1beta1
kind: Gateway
metadata:
  name: my-gateway
  namespace: my-namespace
spec:
  selector:
    istio: ingressgateway
    servers:
        port:
        number: 80
        name: http
        protocol: HTTP
        hosts: "*"
```

Trong đoạn cấu hình trên, chúng ta định nghĩa một Gateway với tên `my-gateway` trong namespace `my-namespace`. Gateway này sẽ áp dụng cho tất cả các host (*) và lắng nghe trên cổng 80 với giao thức HTTP.

Áp dụng cấu hình Gateway bằng lệnh:

```bash
kubectl apply -f my-gateway.yaml
```

#### 3.2.2. Tạo đối tượng VirtualService

Đối tượng VirtualService cho phép bạn định tuyến lưu lượng mạng từ Gateway đến các service trong cluster. Tạo một tệp YAML với nội dung sau:

```yaml
apiVersion: networking.istio.io/v1beta1
kind: VirtualService
metadata:
  name: my-virtual-service
  namespace: my-namespace
spec:
  hosts:
  - "*"
  gateways:
  - my-gateway
  http:
  - match:
    - uri:
        prefix: /app1
    route:
    - destination:
        host: app1-service
        port:
          number: 80
  - match:
    - uri:
        prefix: /app2
    route:
    - destination:
        host: app2-service
        port:
          number: 80
```

Trong đoạn cấu hình trên, chúng ta định nghĩa một VirtualService có tên `my-virtual-service` trong namespace `my-namespace`. VirtualService này áp dụng cho tất cả các host (*) và sử dụng Gateway `my-gateway` đã định nghĩa ở trên. Chúng ta cũng định tuyến lưu lượng mạng từ `/app1` đến service `app1-service` và từ `/app2` đến service `app2-service`.

Áp dụng cấu hình VirtualService bằng lệnh:

```bash 
kubectl apply -f my-virtual-service.yaml
```

Bây giờ, bạn đã cài đặt và cấu hình thành công Istio Ingress Controller trong môi trường Kubernetes của bạn. Istio Ingress Controller sẽ giúp điều hướng lưu lượng mạng từ bên ngoài vào các service trong cluster dựa trên cấu hình của Gateway và VirtualService.

Phần tiếp theo trong outline là "Ứng dụng của Kubernetes Ingress trong thực tế". Trong phần này, chúng ta sẽ đi qua một số ví dụ về cách sử dụng Kubernetes Ingress trong các tình huống thực tế.

## 4. Ứng dụng của Kubernetes Ingress trong thực tế

### 4.1. Load balancing

Ingress giúp cân bằng tải giữa các Pod chạy cùng một service bằng cách phân phối lưu lượng mạng đến các Pod này. Ingress Controller hỗ trợ cân bằng tải theo nhiều thuật toán khác nhau, như round-robin, session affinity, hoặc dựa trên trọng số. Bằng cách sử dụng Ingress, bạn có thể đảm bảo rằng ứng dụng của bạn có khả năng chịu tải cao hơn và phân phối lưu lượng mạng một cách hiệu quả.

### 4.2. Blue/Green deployment

Ingress hỗ trợ Blue/Green deployment, cho phép bạn triển khai hai phiên bản của ứng dụng cùng một lúc và chuyển đổi lưu lượng mạng giữa chúng một cách linh hoạt. Bằng cách sử dụng Ingress, bạn có thể thử nghiệm phiên bản mới của ứng dụng mà không ảnh hưởng đến phiên bản hiện tại, giúp giảm thiểu rủi ro và đảm bảo tính ổn định của hệ thống.

### 4.3. Canary release

Ingress cho phép bạn thực hiện Canary release bằng cách điều hướng một phần nhỏ lưu lượng mạng đến phiên bản mới của ứng dụng, trong khi phần lớn lưu lượng vẫn được gửi đến phiên bản hiện tại. Bằng cách sử dụng Ingress, bạn có thể kiểm tra hiệu năng và tính năng của phiên bản mới trước khi triển khai hoàn toàn, giúp giảm thiểu rủi ro và đảm bảo tính ổn định của hệ thống.

### 4.4. SSL Termination và Passthrough

Ingress hỗ trợ cả SSL Termination và SSL Passthrough. Với SSL Termination, Ingress giải mã lưu lượng SSL/TLS trước khi chuyển tiếp đến service bên trong, giúp giảm tải cho service và đơn giản hóa việc quản lý chứng chỉ SSL/TLS. Trong khi đó, SSL Passthrough cho phép lưu lượng SSL/TLS được chuyển tiếp trực tiếp đến service mà không cần giải mã, giúp đảm bảo bảo mật và tuân thủ các quy định về bảo mật.

### 4.5. Web Application Firewall (WAF)

Một số Ingress Controller hỗ trợ tích hợp với Web Application Firewall (WAF) để bảo vệ ứng dụng của bạn khỏi các mối đe dọa và tấn công từ bên ngoài. WAF giúp phát hiện và ngăn chặn các tấn công như SQL injection, Cross-Site Scripting (XSS), hoặc Distributed Denial of Service (DDoS). Bằng cách sử dụng Ingress, bạn có thể dễ dàng tích hợp WAF vào ứng dụng Kubernetes của mình và cung cấp một lớp bảo mật bổ sung cho service của bạn.

Kết hợp với những ứng dụng đã nêu ở phần trước, Ingress trở thành một công cụ mạnh mẽ và linh hoạt để quản lý và bảo vệ ứng dụng của bạn trong môi trường Kubernetes. Từ cân bằng tải, triển khai ứng dụng linh hoạt, đến bảo mật và tích hợp với các service bên ngoài, Ingress giúp bạn xây dựng và vận hành ứng dụng một cách hiệu quả và an toàn.

## 5. Tối ưu hóa và bảo mật Ingress

Trong chương này, chúng ta sẽ tìm hiểu về cách tối ưu hóa và bảo mật Ingress trong môi trường Kubernetes của bạn.

### 5.1. Tối ưu hóa hiệu năng

- **Sử dụng nén gzip**: Bật nén gzip cho Ingress giúp giảm bớt kích thước dữ liệu được truyền qua mạng, giảm độ trễ và tăng hiệu năng. Hầu hết các Ingress Controller đều hỗ trợ nén gzip, và bạn có thể bật nén gzip bằng cách thêm annotations vào Ingress Resource.
- **Tận dụng HTTP/2**: HTTP/2 cung cấp nhiều cải tiến so với HTTP/1.1, như multiplexing, server push và header compression. Bằng cách sử dụng Ingress Controller hỗ trợ HTTP/2, bạn có thể tận dụng những cải tiến này để tăng hiệu năng ứng dụng của bạn.
- **Caching và CDN**: Tích hợp Ingress với các dịch vụ caching và Content Delivery Network (CDN) giúp giảm tải cho service và tăng tốc độ phân phối nội dung cho người dùng. Bằng cách sử dụng Ingress, bạn có thể dễ dàng tích hợp các dịch vụ này vào ứng dụng của mình.

### 5.2. Bảo mật 

- **Áp dụng Rate Limiting**: Rate limiting giúp hạn chế số lượng yêu cầu mà một người dùng hoặc địa chỉ IP có thể gửi đến ứng dụng của bạn trong một khoảng thời gian nhất định. Điều này giúp ngăn chặn các tấn công từ bên ngoài, giảm tải cho hệ thống và đảm bảo rằng ứng dụng của bạn hoạt động ổn định. Hầu hết các Ingress Controller đều hỗ trợ rate limiting và bạn có thể cấu hình rate limiting thông qua annotations hoặc Custom Resource Definitions (CRDs).

- **Bảo mật người dùng bằng xác thực và ủy quyền**: Ingress cũng hỗ trợ xác thực và ủy quyền cho các service trong Cluster. Bằng cách sử dụng các tính năng như JWT, OAuth2, hoặc xác thực cơ bản (Basic Authentication), bạn có thể bảo vệ các service của mình khỏi truy cập trái phép và đảm bảo rằng chỉ những người dùng được ủy quyền mới có thể truy cập vào service. Bạn có thể cấu hình xác thực và ủy quyền thông qua annotations hoặc CRDs, tùy thuộc vào Ingress Controller mà bạn sử dụng.

Khi kết hợp các biện pháp bảo mật này với những kỹ thuật tối ưu hóa hiệu năng đã đề cập ở phần trước, Ingress sẽ trở thành một công cụ mạnh mẽ giúp bạn xây dựng và vận hành ứng dụng của mình trong môi trường Kubernetes một cách hiệu quả và an toàn.

## 6. Kết luận

Kubernetes Ingress là một công cụ quan trọng và linh hoạt trong việc quản lý lưu lượng mạng và định tuyến cho các ứng dụng chạy trên Kubernetes. Ingress cung cấp nhiều tính năng và ứng dụng hữu ích, giúp bạn cân bằng tải, triển khai ứng dụng linh hoạt, tối ưu hóa hiệu năng, và bảo vệ ứng dụng của mình khỏi các mối đe dọa từ bên ngoài.

Trong bài viết này, chúng ta đã tìm hiểu về các thành phần chính của Ingress, cách cài đặt và cấu hình Ingress, các ứng dụng của Ingress trong thực tế, và các kỹ thuật tối ưu hóa và bảo mật Ingress. Bằng cách áp dụng những kiến thức và kỹ thuật này, bạn sẽ có thể vận hành ứng dụng của mình trên Kubernetes một cách hiệu quả và an toàn.

Nhớ rằng, để tận dụng tối đa các tính năng và ưu điểm của Ingress, bạn cần lựa chọn Ingress Controller phù hợp với nhu cầu và yêu cầu của ứng dụng. Ngoài ra, đừng ngại thử nghiệm và tích hợp các dịch vụ bên ngoài để tối ưu hóa hiệu năng và bảo mật cho ứng dụng của bạn.

Hy vọng thông qua bài viết này, bạn đã có cái nhìn tổng quát và sâu sắc hơn về Kubernetes Ingress và các ứng dụng của nó. Chúc bạn thành công trong việc xây dựng và vận hành ứng dụng của mình trên Kubernetes!

## 7.Tài liệu tham khảo 

1.  Kubernetes (2021). Ingress.  [https://kubernetes.io/docs/concepts/services-networking/ingress/](https://kubernetes.io/docs/concepts/services-networking/ingress/)
2.  Kubernetes (2021). Ingress Controllers.  [https://kubernetes.io/docs/concepts/services-networking/ingress-controllers/](https://kubernetes.io/docs/concepts/services-networking/ingress-controllers/)
3.  Nginx (2021). Nginx Ingress Controller.  [https://www.nginx.com/products/nginx/kubernetes-ingress-controller](https://www.nginx.com/products/nginx/kubernetes-ingress-controller)
4.  Kubernetes (2021). Ingress Resource.  [https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.20/#ingress-v1-networking-k8s-io](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.20/#ingress-v1-networking-k8s-io)
5.  Istio (2021). Istio Ingress Controller.  [https://istio.io/latest/docs/tasks/traffic-management/ingress/](https://istio.io/latest/docs/tasks/traffic-management/ingress/)
6.  Let's Encrypt (2021). Let's Encrypt - Free SSL/TLS Certificates.  [https://letsencrypt.org/](https://letsencrypt.org/)
7.  Helm (2021). Helm - The Kubernetes Package Manager.  [https://helm.sh/](https://helm.sh/)
8.  Envoy (2021). Envoy Proxy.  [https://www.envoyproxy.io/](https://www.envoyproxy.io/)
9.  Kubernetes (2021). Network Policies.  [https://kubernetes.io/docs/concepts/services-networking/network-policies/](https://kubernetes.io/docs/concepts/services-networking/network-policies/)
10.  Kubernetes (2021). Service Mesh.  [https://kubernetes.io/docs/concepts/services-networking/service-mesh/](https://kubernetes.io/docs/concepts/services-networking/service-mesh/)